#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->spinBoxX, SIGNAL(valueChanged(int)), this, SLOT(update()));
    connect(ui->spinBoxY, SIGNAL(valueChanged(int)), this, SLOT(update()));
    connect(ui->spinBoxWidth, SIGNAL(valueChanged(int)), this, SLOT(update()));
    connect(ui->spinBoxHeight, SIGNAL(valueChanged(int)), this, SLOT(update()));

    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::openImageFile);

    connect(ui->saveButton, &QPushButton::clicked, this, &MainWindow::saveImage);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openImageFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png)"));
    if (fileName.isEmpty())
    {
        showErrorDialog("No file selected or file path is empty.");
        imageLoaded = false;
        update();
        return;
    }

    QImage image(fileName);
    if (image.isNull())
    {
        showErrorDialog("The selected file is not a valid image.");
        imageLoaded = false;
        update();
        return;
    }

    QLabel* imageLabel = findChild<QLabel*>("imageLabel");
    if(imageLabel) {
        QPixmap pixmap = QPixmap::fromImage(image);
        QPixmap scaledPixmap = pixmap.scaled(imageLabel->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        imageLabel->setPixmap(scaledPixmap);
        imageLoaded = true;


        ui->spinBoxWidth->setValue(100);
        ui->spinBoxHeight->setValue(100);
        ui->spinBoxX->setValue((imageLabel->width() - 100) / 2 + imageLabel->x());
        ui->spinBoxY->setValue((imageLabel->height() - 100) / 2 + imageLabel->y());
        update();
    }
}

// void MainWindow::saveImage()
// {
//     QLabel* imageLabel = findChild<QLabel*>("imageLabel");
//     if(imageLabel) {
//         QPixmap pixmap = imageLabel->pixmap(Qt::ReturnByValue);
//         if (pixmap.isNull()) {
//             showErrorDialog("No image loaded.");
//             return;
//         }

//         QRect rectangle = getRectangleCoordinates();

//         if (rectangle.isNull()) {
//             showErrorDialog("No rectangle drawn or rectangle is invalid.");
//             return;
//         }

//         QImage image = pixmap.toImage(); // Convertir le QPixmap en QImage

//         QImage croppedImage = image.copy(rectangle);

//         QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save Image"), "", tr("Image Files (*.png)"));
//         if (!saveFileName.isEmpty()) {
//             if (croppedImage.save(saveFileName)) {
//                 QMessageBox::information(this, tr("Success"), tr("Image saved successfully."));
//             } else {
//                 showErrorDialog("Failed to save image.");
//             }
//         }
//     }
// }

void MainWindow::saveImage()
{
    QLabel* imageLabel = findChild<QLabel*>("imageLabel");
    if(imageLabel) {
        QPixmap pixmap = imageLabel->pixmap(Qt::ReturnByValue);
        if (pixmap.isNull()) {
            showErrorDialog("No image loaded.");
            return;
        }

        QImage image = pixmap.toImage(); // Convertir le QPixmap en QImage

        QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save Image"), "", tr("Image Files (*.png)"));
        if (!saveFileName.isEmpty()) {
            if (image.save(saveFileName)) {
                QMessageBox::information(this, tr("Success"), tr("Image saved successfully."));
            } else {
                showErrorDialog("Failed to save image.");
            }
        }
    }
}

QRect MainWindow::getRectangleCoordinates() const
{
    // Récupérer les valeurs des spin boxes
    int x = ui->spinBoxX->value();
    int y = ui->spinBoxY->value();
    int width = ui->spinBoxWidth->value();
    int height = ui->spinBoxHeight->value();

    // Créer et retourner le rectangle
    return QRect(x, y, width, height);
}


void MainWindow::showErrorDialog(const QString &message)
{
    QMessageBox::critical(this, tr("Error"), message);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QMainWindow::paintEvent(event);

    QPainter painter(this);
    QRect rect(ui->spinBoxX->value(), ui->spinBoxY->value(), ui->spinBoxWidth->value(), ui->spinBoxHeight->value());
    painter.setPen(QPen(Qt::red, 2));
    painter.drawRect(rect);
}
