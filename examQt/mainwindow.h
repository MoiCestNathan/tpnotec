#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QImage>
#include <QLabel>
#include <QMessageBox>
#include <QPainter>
#include <QSpinBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void openImageFile();
    void showErrorDialog(const QString &message);
    void paintEvent(QPaintEvent *event);
    void saveImage();


private:
    Ui::MainWindow *ui;
    QLabel *imageLabel;
    bool imageLoaded = false;
    QRect getRectangleCoordinates() const;

};

#endif // MAINWINDOW_H
